import React, { Component } from 'react';
import { View, Text, Image, FlatList, StyleSheet, TouchableOpacity } from 'react-native';
import {images} from '../Components/dummyData';
import AsyncStorage from '@react-native-community/async-storage';

export default class myPrescriptions extends Component {
  constructor(props) {
    super(props);
    this.state = { dataSource: [] };
  }

  fetchData = async () => {
    let user = await AsyncStorage.getItem('userdetails');
    user = JSON.parse(user);
    //let user_id= setUserid(user.id);
    console.log('user id=>', user.id);
    let fetchUrl = 'http://3.12.158.241/medical/api/userImage/'+user.id;
    console.log('Image url------>', fetchUrl);
    const response = await fetch(fetchUrl);
    const json = await response.json();
    this.setState({media: json.media});
    console.log('media',this.state.media);
    //var myDataArray = json.data;
    //console.log('cgfgfg-->',myDataArray);
    //myDataArray.length === 0 ? this.props.navigation.navigate('AddAdressScreen'): '';
  };

  renderItem = ({item}) => {
    console.log('image', item.id)
    return(
    // <View style={{flexDirection: 'row'}}>
          <Image source = {{ uri: item.image }} style={{height: 60, width: 60}}/>
          //</View>
    );
  }

  async componentDidMount() {
    let user = await AsyncStorage.getItem('userdetails');
    user = JSON.parse(user);
    const response = await fetch('http://3.12.158.241/medical/api/userImage/'+user.id);
    const responseJson = await response.json();
    this.setState({ dataSource: responseJson.media });
  }

  render() {
    return (
      <View style={{marginTop: 20}}>
        
        {/* <Image source = {{ uri: 'http://3.12.158.241/medical/images/60bf0bdbc4ed5.jpg'}} style={{height: 100, width: 100}}/> */}
        <FlatList
          data={this.state.dataSource}
          renderItem={({item}) =><View style={{flexDirection:'row'}}> 
          <Image source = {{ uri: item.image}} style={styles.images}/>
          </View>}
          keyExtractor={({item})=>this.state.dataSource.indexOf(item)}
          numColumns={3}
        />
        
    <View style={{alignItems: 'center'}}>
      <TouchableOpacity onPress={this.fetchData} style={styles.appButtonContainer}>
         <Text style={styles.appButtonText}>Done</Text>
      </TouchableOpacity>
    </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  appButtonContainer: {
    elevation: 8,
    backgroundColor: "#1e90ff",
    borderRadius: 50,
    paddingVertical: 10,
    paddingHorizontal: 12,
    marginTop: 250,
    width: '90%',
    marginLeft: 10,
    marginRight: 10,
  },
  appButtonText: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "uppercase"
  },
  images: {
    height: 100,
    width: 100,
    marginLeft: 20,
    
  }
});

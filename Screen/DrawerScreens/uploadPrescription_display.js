import React, {Component} from 'react';
import {
    View, Text, StyleSheet, ScrollView, Alert,
   Image, TouchableOpacity, NativeModules, Dimensions, StatusBar, SafeAreaView
} from 'react-native';
//import {CarColors} from "../assets/Colors";

//var commonStyles = require('../assets/style');
var ImagePicker = NativeModules.ImageCropPicker;


export default class App extends Component {

    constructor() {
        super();
        this.state = {
            image: null,
            images: null
        };
    }

    cleanupImages() {
        ImagePicker.clean().then(() => {
            // console.log('removed tmp images from tmp directory');
            alert('Temporary images history cleared')
        }).catch(e => {
            alert(e);
        });
    }

    pickMultiple() {
        ImagePicker.openPicker({
            multiple: true,
            waitAnimationEnd: false,
            includeExif: true,
            forceJpg: true,
        }).then(images => {
            this.setState({
                image: null,
                images: images.map((item,index) => {
                    //console.log('received image', i);
                    data.append("doc[]", {
                        uri: item.path,
                        type: "image/jpeg",
                        name: item.filename || 'temp_image_${index}.jpg',
                    })
                    return {uri: item.path, width: item.width, height: item.height, mime: item.mime};
                })
            });
        }).catch(e => alert(e));
    }

    upload = () => {
        //console.log(JSON.stringify(data));
    
          fetch('www.fetch.api',{
              method: "post",
              headers: {
                  Accept : "application/x-www.form-urlencoded",
              },      
              body: data,
      }).then(res => res.json())
      .then(res => {
          alert("Success")
      }).catch(err => {
         console.error("error uploading images:", err);
      })
      }

    scaledHeight(oldW, oldH, newW) {
        return (oldH / oldW) * newW;
    }

    renderImage(image) {
        return <Image style={{width: 200, height: 200, resizeMode: 'contain'}} source={image}/>
    }

    renderAsset(image) {
        if (image.mime && image.mime.toLowerCase().indexOf('video/') !== -1) {
            return this.renderVideo(image);
        }

        return this.renderImage(image);
    }

    render() {
        return (
            <SafeAreaView style={styles.safeArea}>

                <View style={styles.container}>
                    <StatusBar
                        //backgroundColor={CarColors.primary}
                        barStyle="light-content"/>
                    <TouchableOpacity onPress={this.pickMultiple.bind(this)} >
                        <Text >Select Images</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.cleanupImages.bind(this)}>
                        <Text>Clean History</Text>
                    </TouchableOpacity>
                </View>

                <ScrollView style={styles.imgContainer}>
                    {this.state.image ? this.renderAsset(this.state.image) : null}
                    {this.state.images ? this.state.images.map((item,index) => <View style={styles.imgView}
                                                                          key={item.uri}>{this.renderAsset(item)}</View>) : null}
                    {
                        this.state.images &&
                        <TouchableOpacity onPress={this.upload()} >
                            <Text>Upload</Text>
                        </TouchableOpacity>
                    }
                </ScrollView>


            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
    },
    imgContainer: {
        marginVertical: 40,
        //marginHorizontal: 40,
    },
    button: {
        backgroundColor: 'blue',
        marginBottom: 10,
    },
    text: {
        color: 'white',
        fontSize: 20,
        textAlign: 'center'
    },
    title: {
        fontWeight: 'bold',
        fontSize: 22
    },
    safeArea: {
        marginTop: 20
    },
    dateContainer: {
        flexDirection: 'row',
    },
    imgView: {
        width: '50%',
        marginVertical: 10,
      

    }
});